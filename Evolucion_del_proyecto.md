# Biblioteca Enlace Libre - Django

## ¿Qué es?
Es una web realizada en Django para poder subir libros con Licencias Libres.
Al día de hoy podés:
-Registrarte con tu usuario.
-Subir un libro en PDF con su imagen en JPG o PNG.
-Bajarte todos los libros que se encuentren en línea sin registro requerido.

## Informacion principal - Historia
Este proyecto surgió a partir de la materia "Culturas Digitales Libres" de la Universidad Nacional del Litoral, donde me encuentro realizado la Tecnicatura en Software Libre.
Actualmente está montado en una Raspberri Pi e inicialmente fue realizado en en el marco de trabajo Laravel de PHP. Podés verlo en funcionamiento en https://biblioteca-enlacelibre.enlacepilar.com.ar/ 

A partir de ver en la materia Programación 2 el marco de trabajo Django, la idea es trasladar este proyecto y comprobar su rendimiento, subiéndolo al mismo lugar.
Si la cosa funciona, voy a seguir con Django y dar de baja el proyecto en Laravel.

## Etapas
1) Clonado el repo.
2) Genero la app de biblioteca donde va a estar contenida.
3) Copio los archivos de blade para adaptar a las plantillas Django.
4) Adapto barra superior y plantilla raiz.
5) Creo modelos de Generos y Libros
6) Creo app de usuarios para logueo.
7)
###Junio 2022
============
-Separo en aplicaciones todo el contenido que traje de Laravel

    a) Ayuda: para las páginas que tienen que ver con el proyecto en sí. En la barra principal se puede acceder a ellas a través de la sección' Info'.
    
    b) Biblioteca: se encarga de listar los libros subidos a nivel de usuario registrado.
    
    c) Buscador: Maneja los resultados de los libros que se realizan en el campo de búsqueda de la página principal.
    
    d) Blog: en proceso, posiblemente con las novedades. Basado en la app del blog genérico de webempresa.
    
    e) Libros: Maneja el ABM de los libros y el modelo con los géneros. Lista los libros por género también.
    
    f) Media: donde se encuentran las imágenes de los libros y los PDFs.
    
    g) Página de inicio: Maneja el inicio, la plantilla general y la barra superior. Lista los últimos libros subidos. 
    h) Perfiles: se encarga de las modificaciones de los perfiles de los usuarios.
    
    i) Principal: Es el proyecto principal con todas las configuraciones iniciales.
    
    j) Usuarios: maneja el login, creación de usuarios y logout.
-Genero un archivo requirements.txt para poder instalar las dependencias sin problemas
(puede que haya un conflicto con pillow, la dependencia que maneja las imagenes, pero se soluciona asi:
python -m pip install -U pip
python -m pip install -U pillow
encontré la solución aca: https://github.com/python-pillow/Pillow/issues/4158
)

8)Me baso en el blog de Webempresa para la aplicación de blog. Desde el admin se pueden crear entradas que aparecerán listadas en la sección /blog. Tuve varios problemas con la ruta media hasta que más o menos le enganché la mano :)

9)Pongo en funcionamiento la sección de Info(app ayuda): Proyecto, Editá, Colaborá y Cultura Libre, además del Blog, proveniente de su correspondiente sección.

####22-06-22

10)Listo en la página principal los libros que momentáneamente pueden cargarse desde el sector de administración de Django. Momentáneamente agrego en cada vista posea la barra superior los generos que se obtienen de la base de datos.

11) Para mejorar el formulario instalar: pip install django-crispy-forms --No aparece en requirements.txt, no sé por qué.

12) ¡Ya pueden cargarse libros desde un formulario!

###26-06-22

13) Mejoro la vista inicial de los libros con ListView, con paginado incluido. Creo un pequeño script para poder insertar libros genéricos. Al menos 2 deben insertarse antes en forma normal. Después copiar las rutas de las tapas y los PDFs para que las inserciones sean reales a la vista. :)

###1-7-22

14) Creo los generos basado en vistas.

15) Se puede actualizar el perfil del usuario, correo y nombre. Por el momento generado en estilo clásico. Resta ver la sanitización del mismo en ese modo.

###2-7-22

16) Realizo en la app biblioteca un crud basado en vistas. Solo el listado de los libros por usuarios, la edición y el borrado. Cargar libro está en la app libros". :)

###5-7-22

17) Ya se puede modificar el perfil de usuario con un mensaje de éxito y deriva a la página del listado de libros. Agrego este mismo mensaje cuando se actualiza la info de los libros. Ambos vistas están basadas en ListViews. 

###6-7-22

18) Después de volverme loco tratando de listar los géneros en la barra superior, ¡lo conseguí de una vez haciendo una consulta asíncrona! :)

###12-7-22

19) Termino de filtrar en una tabla los los libros por géneros a través de la barra superior, siguiendo con los pasos del punto 18. 

###27-7-22

20) Termino el buscador de la página principal. Ordeno la visualización de los libros también. 

21) Restrinjo subidas de libros a: sólo PDF de hasta 50mb para los libros y JPG/PNG de hasta 5 para las tapas con el respectivo validador. 

###4-3-23

22) Corrijo algunos detalles menores. Agrego Django-environ para proteger claves de acceso a la base de datos.

23) Por sugerencia de la profe Ana Almada de programacion 2 de la UNL, corrijo Readme con mayor detalle para su instalacion y clonado en cualquier equipo. Gracias profe!
