# Biblioteca Enlace Libre - Django


## ¿Qué es?
Es una web realizada en Django para poder subir libros con Licencias Libres (idea principal).
Al día de hoy podés:
-Registrarte con tu usuario.
-Subir un libro en PDF con su imagen en JPG o PNG.
-Bajarte todos los libros que se encuentren en línea sin registro requerido.


## Informacion técnica
Realizado en Django 3.2.13 (puede funcionar con la 4). Necesita conexión a Mysql o Maríadb. Versión de Python: 3.10


## Estructura de la aplicación (describo las funcionalidad de cada app creada)

* Ayuda: para las páginas que tienen que ver con el proyecto en sí. En la barra principal (html) se puede acceder a ellas a través de la sección' Info'.
    
* Biblioteca: se encarga de listar los libros subidos a nivel de usuario registrado.
    
* Buscador: Maneja los resultados de los libros que se realizan en el campo de búsqueda de la página principal.
    
* Blog: en proceso, posiblemente con las novedades. Basado en la app del blog genérico de webempresa.
    
* Libros: Maneja el ABM de los libros y el modelo con los géneros. Lista los libros por género también. Allí se encuentran esos dos modelos diseñados en models.
    
* Media: donde se encuentran las imágenes de los libros y los PDFs.
    
* Página de inicio: Maneja el inicio, la plantilla general y la barra superior. Lista los últimos libros subidos. 

* Perfiles: se encarga de las modificaciones de los perfiles de los usuarios.
    
* Principal: Es el proyecto principal con todas las configuraciones iniciales.
    
* Usuarios: maneja el login, creación de usuarios y logout.


## Instalación y puesta en marcha

* Clonar el repo: git clone https://gitlab.com/enlacepilar/enlace-libre-django.git

* Crear un entorno aislado con Python: python -m venv envBiblio

* Iniciar el entorno: source envBiblio/bin/activate

* Instalar los requeremientos de la app: pip install -r requerimientos.txt

* Ir a la carpeta principal/principal y copiar el archivo env(copia): cp env(copia) .env

* Editar "env" creado con los datos de a la ssecret key y el usuario y clave y nombre de de la base de datos: nano .env

* Creamos abriendo otra consola la clave en base64: openssl rand -base64 32

* Esta clave la pegamos en Secret Key del .env

* Subimos un nivel y donde se encuentra "manage.py" ejecutamos: ***python manage.py make migrations*** y luego ***python manage.py migrate***

* Creamos nuestro super usuario o usuario administrador: python manage.py createsuperuser

* Ejecutamos el entorno: python manage.py runsever

* Ingresamos por navegador a localhost:8000/admin, ponemos usuario y clave creados y acto seguido, en el sector de géneros, creamos algun género, como puede ser: poesía, cuento, novela...

* Volvemos a localhost:8000 y podremos verlo reflejado en "Generos" de la barra superior.


## Aclaración:

Las publicaciones del blog, a modo informativo, sólo podrán darse de alta desde el entorno Admin.


## Posibles alcances

Cabe aclarar que esta app no solo está disponible para subir PDFs de libros, puede ser adaptada a sus necesidades para catálogos, listados, manuales, etc. Sólo requerirá unas pocas modificaciones.


## Contacto y web de ejemplo disponible:

Correo: enlacepilar@protonmail.com

Biblioteca EnlaceLibre: https://biblioteca-enlacelibre.enlacepilar.com.ar/

