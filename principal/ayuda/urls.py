from django.urls import path
from . import views


urlpatterns = [
    path('proyecto', views.proyecto, name="proyecto"),
    path('edita', views.edita, name="edita"),
    path('colabora', views.colabora, name="colabora"),
    path('cultura-libre', views.culturalibre, name="culturalibre"),
   
    
]