from django.shortcuts import render


def proyecto(request):
   
    return render(request, 'proyecto.html')

def edita(request):
   
    return render(request, 'edita.html')

def colabora(request):
   
    return render(request, 'colabora.html')

def culturalibre(request):
   
    return render(request, 'cultura-libre.html')
