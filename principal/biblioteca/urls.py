from django.urls import path


from . import views
from .views import LibrosUsuario, LibroActualizar, LibroBorrar


urlpatterns = [
    path('', LibrosUsuario.as_view(), name='libros_lista'),
    path('actualizar/<int:pk>', LibroActualizar.as_view(), name='libro_actualizar'),
    path('borrar/<int:pk>', LibroBorrar.as_view(), name='libro_borrar'),   
    
]


 