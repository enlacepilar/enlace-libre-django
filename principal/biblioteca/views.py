from django.shortcuts import render

from django.views.generic import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    CreateView,
    UpdateView,
    DeleteView
)
#from django.core.urlresolvers import reverse_lazy #Version vieja
from django.urls import reverse_lazy
from django.contrib import messages

from django.shortcuts import redirect

from libros.models import Libros

class LibrosUsuario(ListView):
    template_name = 'libros-por-usuario.html'
    model = Libros
    paginate_by = 4
    context_object_name = 'libros'
    
    def get_queryset(self, **kwargs):
        return Libros.objects.filter(usuario_id_id=self.request.user.id)
    
    
    # def get_queryset(self):
    #     return Libros.objects.filter(usuario_id_id=self.request.user.id)

# class LibroDetalle(DetailView):
#     model = Libros


# class LibroCargar(CreateView):
#     model = Libros
#     success_url = redirect('biblioteca')
#     fields = ['titulo', 'autor', 'imagen', 'descripcion']

class LibroActualizar(UpdateView):
    model = Libros
    template_name = 'libro-editar.html'
    fields = ['titulo', 'autor', 'descripcion']
    success_url = reverse_lazy('libros_lista')
    
    def form_valid(self, form):
      messages.success(self.request, "¡La info del libro fue actualizada!")
      super().form_valid(form)
      #return HttpResponseRedirect(self.get_success_url())
      return redirect ('/biblioteca')
    
class LibroBorrar(DeleteView):
    model = Libros
    template_name = 'confirma-borrado.html'
    success_url = reverse_lazy('libros_lista')