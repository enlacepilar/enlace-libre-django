from django.contrib import admin
from .models import Publicacion, Categoria

# Register your models here.

class PublicacionAdmin(admin.ModelAdmin):
	readonly_fields = ['creado', 'actualizado']
	list_display = ('titulo', 'autor', 'publicado', 'publicaciones_categorias')
	ordering = ('autor', 'publicado')
	search_fields = ('titulo','autor__username', 'categorias__nombre')
	date_hierarchy = 'publicado'
	list_filter = ('autor__username','categorias__nombre')

	def publicaciones_categorias(self, obj):
		return ", ".join([c.nombre for c in obj.categorias.all().order_by("nombre")])
	publicaciones_categorias.short_description = "Categorías"

admin.site.site_header = "Administrador"
admin.site.register(Publicacion, PublicacionAdmin)
admin.site.register(Categoria)