from django.db import models
from django.utils.timezone import now
from django.contrib.auth.models import User
# Create your models here.

class Categoria(models.Model):
	nombre = models.CharField(max_length=100, verbose_name='Nombre')

	class Meta:
		verbose_name = 'categoria'
		verbose_name_plural = 'categorias'

	def __str__(self):
		return self.nombre

class Publicacion(models.Model):
	titulo = models.CharField(max_length=200, verbose_name='Título')
	imagen = models.ImageField(upload_to="blog", null=True, blank=True, verbose_name='Imagen')
	contenido = models.TextField(verbose_name='Contenido')
	publicado = models.DateTimeField(verbose_name='Fecha de publicación', default=now)
	categorias = models.ManyToManyField(Categoria, verbose_name='Categoría', related_name='traer_publicaciones')
	autor = models.ForeignKey(User, verbose_name='Autor', on_delete=models.CASCADE)
	creado = models.DateTimeField(auto_now_add=True, verbose_name='Fecha creación')
	actualizado = models.DateTimeField(auto_now=True)

	class Meta:
		verbose_name = 'entrada'
		verbose_name_plural = 'entradas'

	def __str__(self):
		return self.titulo