from django.shortcuts import render, get_object_or_404
from .models import Categoria, Publicacion


# Create your views here.
def blog(request):
	publicaciones = Publicacion.objects.all()

	return render(request, "blog/blog.html", {'publicaciones':publicaciones})

def categoria(request, categoria_id):
	# category = Category.objects.get(id=category_id)
	categoria = get_object_or_404(Category, id=categoria_id)
	return render(request, "blog/categoria.html", {'categoria':categoria})