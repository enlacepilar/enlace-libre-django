from django.urls import path
from . import views

urlpatterns = [
    path('', views.resultadosBuscador, name='resultadosBuscador')
]