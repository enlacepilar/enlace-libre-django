from django.shortcuts import render
from libros.models import Libros
from django.core.paginator import Paginator


from django.http import HttpResponse


def resultadosBuscador(request):
   titulo_o_autor = request.GET.get('tituloOAutor')
   buscar = request.GET.get('buscaLibro')
   generoBuscar = request.GET.get('buscaGenero')
   if generoBuscar != "nada":
      if titulo_o_autor == "titulo":
         libros = Libros.objects.filter(titulo__icontains=buscar, genero_id=generoBuscar)
      else:
         libros = Libros.objects.filter(autor__icontains=buscar, genero_id=generoBuscar)
   else: 
      if titulo_o_autor == "titulo":
         libros = Libros.objects.filter(titulo__icontains=buscar)
      else:
         libros = Libros.objects.filter(autor__icontains=buscar)

      
   paginator = Paginator(libros, 10)
   page_number = request.GET.get('page')
   page_obj = paginator.get_page(page_number)
   return render (request, 'resultados-buscador.html', {'libros': libros, 'titulo_o_autor':titulo_o_autor, 'buscar':buscar, 'page_obj': page_obj})