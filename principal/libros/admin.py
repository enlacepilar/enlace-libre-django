from django.contrib import admin

# Register your models here.
from .models import Generos, Libros 


admin.site.register(Generos)
admin.site.register(Libros)