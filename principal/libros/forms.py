from django import forms
from .models import Generos, Libros


class LibrosForm(forms.ModelForm):
    enlace_libro = forms.FileField(label='Agregar Libro en PDF (Máximo 50 mb)', widget=forms.FileInput(attrs={"class":"form-control alert-info", "required": True}))
    imagen = forms.FileField(label='Agregar Imagen JPG/PNG (Máximo 5 MB)', widget=forms.FileInput(attrs={"class":"form-control alert-info", "required": True}))
    #usuario_id = forms.CharField(widget=forms.HiddenInput()) 
    
    class Meta:
        model = Libros
        fields = ['titulo', 'genero_id', 'autor','enlace_libro','imagen','descripcion']
        #widgets = {'usuario_id': forms.HiddenInput()} ##No me funciona si lo oculto
        #help_texts = {k:"" for k in fields }




     