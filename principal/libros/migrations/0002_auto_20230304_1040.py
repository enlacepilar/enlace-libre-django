# Generated by Django 3.2.13 on 2023-03-04 13:40

from django.db import migrations, models
import libros.validador


class Migration(migrations.Migration):

    dependencies = [
        ('libros', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='libros',
            name='enlace_libro',
            field=models.FileField(blank=True, upload_to='libros', validators=[libros.validador.valida_PDF, libros.validador.limite_PDF]),
        ),
        migrations.AlterField(
            model_name='libros',
            name='imagen',
            field=models.ImageField(blank=True, upload_to='imagen_libros', validators=[libros.validador.valida_TAPA, libros.validador.limite_TAPA], verbose_name='Imagen'),
        ),
    ]
