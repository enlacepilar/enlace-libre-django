from django.db import models

from django.conf import settings
from django.utils import timezone

from .validador import *

class Generos (models.Model):
    nombre = models.CharField(max_length=200)
    fecha_creacion = models.DateTimeField(
        default=timezone.now
    )
    fecha_publicacion = models.DateTimeField(
        blank=True, null=True
    )
    def __str__(self):
        #return self.titulo
        return self.nombre

class Libros (models.Model):
    titulo = models.CharField(max_length=200)
    usuario_id = models.ForeignKey(settings.AUTH_USER_MODEL, null=False, on_delete=models.DO_NOTHING)
    genero_id = models.ForeignKey(Generos, null=True, on_delete=models.DO_NOTHING)
    autor = models.CharField(max_length=200)
    enlace_libro = models.FileField(upload_to='libros', null=False, blank=True, validators=[valida_PDF, limite_PDF]) 
    imagen = models.ImageField(upload_to="imagen_libros", null=False, blank=True, verbose_name='Imagen', validators=[valida_TAPA, limite_TAPA])
    descripcion = models.TextField(null=False, verbose_name="Descripcion")
    fecha_creacion = models.DateTimeField(
        default=timezone.now
    )
    fecha_publicacion = models.DateTimeField(
        blank=True, null=True
    )

    def publicar (self):
        self.fecha_publicacion = timezone.now()
        self.save()

    def __str__(self):
        return self.titulo
        #return self.nombre
