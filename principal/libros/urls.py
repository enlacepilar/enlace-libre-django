from django.urls import path
from . import views
#from .views import GeneroVista

urlpatterns = [
    path('cargarLibro', views.cargarLibro, name="cargarLibro"),
    #path('<string>', GeneroVista.as_view(), name='listado_por_genero'),
    path('<str:genero_seleccionado>', views.listadoPorGenero, name='listado_por_genero'),
]