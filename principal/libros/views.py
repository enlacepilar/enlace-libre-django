from django.shortcuts import render, redirect
from .forms import LibrosForm
from .models import Libros, Generos

from django.http import HttpResponse
from django.contrib import messages
from django.views.generic import ListView

from django.core.paginator import Paginator



# Create your views here.
def cargarLibro(request):
    if request.method == "POST":
        formuLibro = LibrosForm(request.POST, request.FILES)
    
        if formuLibro.is_valid():
            #formu = formuLibro.cleaned_data
            formu = formuLibro.save(commit=False)
            formu.usuario_id_id = request.user.id  
            if formu.usuario_id_id == None:
                return HttpResponse('No estas logueado' )   
            else:
                formu.save()
                messages.success(request, f'Libro creado.')
            return redirect ('/')
    else:
        formuLibro = LibrosForm()
    
    return render (request, 'cargarLibro.html', {'form': formuLibro})

# class GeneroVista(ListView):
#     template_name = 'listadoPorGenero.html'
#     model = Libros
#     paginate_by = 10
#     context_object_name = 'libros'
    
#     def get_queryset(self, **kwargs):
#         return Libros.objects.filter(genero_id=self.request.user.id)

def listadoPorGenero(request, genero_seleccionado):
    generoId = Generos.objects.get(nombre = genero_seleccionado).id
    genero = genero_seleccionado
    libros = Libros.objects.filter(genero_id=generoId) 
    paginator = Paginator(libros, 10)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    #return HttpResponse (generoId)
    return render (request, 'listadoPorGenero.html', {'libros': libros, 'genero': genero, 'page_obj': page_obj})
    


