from django.apps import AppConfig


class LibrosExternosConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'libros_externos'
