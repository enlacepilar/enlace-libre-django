from django.urls import path
from . import views

urlpatterns = [
    path('', views.formularioExternos, name='formularioExternos'),
    path('recibeExternos', views.recibeExternos, name='recibeExternos'),
]