from django.urls import path
from . import views
from .views import ListaLibros

from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.urls import include


urlpatterns = [
    
    path('', ListaLibros.as_view()),
    #path('barrasup', ListaGeneros.as_view()),
    path('buscaGeneros', views.traeGeneros, name="traeGeneros")
    
]