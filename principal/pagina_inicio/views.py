from django.views.generic import ListView
from django.shortcuts import render
#from django.http import HttpResponse
from libros.models import Generos, Libros
from django.http import JsonResponse


class ListaLibros(ListView):
    template_name = 'inicio.html'
    model = Libros
    paginate_by = 4
    context_object_name = 'libros'
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        generos = Generos.objects.all()
        #context['filter'] = self.filterset
        context['generos'] = generos  # this line added
        return context
    
    def get_queryset(self):
        return Libros.objects.all().order_by('-id')
    

# class ListaGeneros(ListView):
#     template_name = 'layouts/barraSuperior.html'
#     model = Generos
#     context_object_name = 'generos'

def traeGeneros(request):
    generos = Generos.objects.values_list('nombre')
    generosL = list(generos)
    dicGeneros={}
    dicGeneros["nombre"] = generosL
    return JsonResponse(dicGeneros)
    