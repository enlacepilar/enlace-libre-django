from django.urls import path
from . import views


from .views import UsuarioActualizar


urlpatterns = [
    #path('', PerfilUsuario.as_view()),
    path('', views.perfilUsuario, name="perfilUsuario"),  
    path('actualizar', views.actualizarPerfil, name="actualizarPerfil"), 
    path('<int:pk>', UsuarioActualizar.as_view(), name='libros_lista'),
   

]