from django.shortcuts import render, redirect
from django.views.generic import ListView
from django.contrib.auth.models import User

from django.http import HttpResponse
from django.contrib import messages

from django.views.generic.edit import UpdateView
from django.urls import reverse_lazy



def perfilUsuario(request):
    usu_id = request.user.id 
    usuario = User.objects.get(id=usu_id)
    return render (request, 'modificar-datos-usuario.html', {'usuario': usuario})

def actualizarPerfil(request):
    usu_id = request.user.id 
    nombre = request.POST['nombre']
    correo = request.POST['correo']
    usuario = User.objects.get(id=usu_id)
    usuario.username = nombre
    usuario.email = correo
    usuario.save()

    messages.success(request, f'¡Datos actualizados!')
    return redirect ('/')

 

class UsuarioActualizar(UpdateView):
    model = User
    template_name = 'modificar-datos-usuario.html'
    fields = ['username', 'first_name', 'last_name', 'email']
    success_url = reverse_lazy('libros_lista')
    
    def form_valid(self, form):
      messages.success(self.request, "¡Tus datos fueron actualizados!")
      super().form_valid(form)
      #return HttpResponseRedirect(self.get_success_url())
      return redirect ('/biblioteca')
