from django.urls import path
from . import views

from django.views.generic.edit import CreateView
from django.contrib.auth.forms import UserCreationForm
from django.urls import include


urlpatterns = [
    path('registrar', views.RegistroUsuario, name="registrar"),
    path('salir', views.saleUsuario, name="salir"),
    path('accounts/', include('django.contrib.auth.urls')),
]


