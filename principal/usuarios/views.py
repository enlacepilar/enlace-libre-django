from django.shortcuts import render, redirect
from .models import *

from .forms import UserRegisterForm 
from django.contrib import messages
from django.contrib.auth import logout


from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm

def RegistroUsuario(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            print (form)
            username = form.cleaned_data['username']
            messages.success(request, f'Usuario {username} creado.')
            form.save()
            return redirect ('/')
    else:
        form = UserRegisterForm()
    
    context = { 'form' : form }
    return render(request, 'registro.html', context)

def saleUsuario (request):
    logout(request)
    messages.success(request, f'Saliste de tu sesión correctamente.')
    return redirect ('/')